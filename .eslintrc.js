require("@html-validate/eslint-config/patch/modern-module-resolution");

module.exports = {
	extends: ["@html-validate"],

	overrides: [
		{
			files: "*.ts",
			extends: ["@html-validate/typescript"],
		},
		{
			files: "*.test.ts",
			env: {},
			plugins: [],
			extends: [],
			rules: {},
		},
	],
};
