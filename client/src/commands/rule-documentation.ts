import vscode, {
	Uri,
} from "vscode"; /* eslint-disable-line import/no-unresolved -- will be resolved when running inside vscode */

export function ruleDocumentation(url: string): void {
	if (!url) {
		return;
	}
	vscode.env.openExternal(Uri.parse(url));
}
