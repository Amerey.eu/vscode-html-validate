/* eslint-disable no-console -- for debugging */

import path from "path";
import { runTests } from "@vscode/test-electron";

const rootDir = path.resolve(__dirname, "../../..");
const userDataDir = path.resolve(rootDir, "profile");
const extensionDevelopmentPath = rootDir;
const extensionTestsPath = __dirname;

async function main(): Promise<void> {
	try {
		await runTests({
			version: process.env.VSCODE_VERSION || "stable",
			extensionDevelopmentPath,
			extensionTestsPath,
			launchArgs: [
				`--user-data-dir=${userDataDir}`,

				/* disable other extensions so they dont interfere (e.g. eslint for javascript files) */
				"--disable-extensions",

				/* disable hardware acceleration */
				"--disable-gpu",
				"--disable-gpu-sandbox",
			],
		});
	} catch (err) {
		console.error(err);
		console.error("Failed to run tests");
		process.exitCode = 1;
	}
}

main();
