import path from "path";
import Mocha from "mocha";
import { glob } from "glob";

export async function run(): Promise<void> {
	const mocha = new Mocha({
		ui: "bdd",
		color: true,
	});
	mocha.timeout(30000);

	const testsRoot = process.env.TESTS_ROOT || path.resolve(__dirname, "..");

	const files = await glob("**/**.test.js", { cwd: testsRoot });
	for (const filename of files) {
		mocha.addFile(path.resolve(testsRoot, filename));
	}

	return new Promise((resolve, reject) => {
		try {
			mocha.run((failures) => {
				if (failures > 0) {
					reject(new Error(`${failures} tests failed.`));
				} else {
					resolve();
				}
			});
		} catch (err) {
			reject(err);
		}
	});
}
