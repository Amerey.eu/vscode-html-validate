import { HoverParams, Hover, Range, Position } from "vscode-languageserver";
import { RuleDocumentation } from "html-validate";
import { ApiContext } from "./api-context";
import { ExtendedDiagnostic } from "./extended-diagnostic";

function containsPosition(range: Range, position: Position): boolean {
	if (position.line < range.start.line || position.line > range.end.line) {
		return false;
	}
	if (position.line === range.start.line && position.character < range.start.character) {
		return false;
	}
	if (position.line === range.end.line && position.character > range.end.character) {
		return false;
	}
	return true;
}

/**
 * Show extended documentation for a diagnostic message.
 */
export async function onHover(this: ApiContext, params: HoverParams): Promise<Hover | null> {
	const resource = params.textDocument.uri;
	const diagnostics: ExtendedDiagnostic[] = await this.getDocumentDiagnostics(resource);
	if (!diagnostics || diagnostics.length === 0) {
		return null;
	}

	const diagnostic = diagnostics.find((cur) => {
		return cur.documentation && containsPosition(cur.range, params.position);
	});
	if (!diagnostic) {
		return null;
	}

	const htmlvalidate = await this.getDocumentValidator(resource);
	if (!htmlvalidate) {
		return null;
	}

	const ruleId = diagnostic.code as string;
	const documentation = diagnostic.documentation as RuleDocumentation;

	const contents = [
		`HTML-Validate`,
		documentation.description,
		`[${ruleId}](${documentation.url})`,
	].join("\n\n");

	return { contents };
}
